This repository contains additional magic files for the [fine file command][file].

In this context, the term [magic][magic] refers to the uses of magic bytes in
file formats to encode the format type, version and other attributes. A more
boring synonym for [magic][magic2] bytes is: file signature

2016, Georg Sauthoff <mail@georg.so>

## Examples

Call [file][file2] with additional magic:

    $ file -m /usr/share/misc/magic.mgc:coverage.magic somefile

Add magic via the `MAGIC` environment variable:

    $ export MAGIC=/usr/share/misc/magic.mgc:/path/to/coverage.magic
    $ file somefile

Tell file to also look into compressed files:

    $ file -z somefile

Compile multiple magic into a preprocessed file:

    $ file -C -m coverage.magic ber.magic
    $ cp magic.mgc /path/to/extra.mgc
    $ export MAGIC=/usr/share/misc/magic.mgc:/path/to/extra.mgc
    $ file somefile


## Rationale

Some files of this repository are already merged with the upstream magic, but
they are collected here for use with older versions of the fine file command.


## Content

- coverage.magic - magic for different coverage data formats that are created by GCC/GCOV/LCOV/Clang
- ber.magic - magic for several ASN.1 BER formats that are used in the mobile telecommunications industry

Both files were merged upstream on 2016-06-06. See also the commit messages of
these files in this repository for details.


[file]: http://www.darwinsys.com/file/
[file2]: https://en.wikipedia.org/wiki/File_(command)
[magic]: https://en.wikipedia.org/wiki/Magic_number_(programming)#Magic_numbers_in_files
[magic2]: https://en.wikipedia.org/wiki/File_format#Magic_number
